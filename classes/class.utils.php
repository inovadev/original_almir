<?


class Utilidades {
		
		function checkEmail($eMailAddress) {
			if (eregi("^[0-9a-z]([-_.]?[0-9a-z])*@[0-9a-z]([-.]?[0-9a-z])*\\.[a-z]{2,3}$", $eMailAddress, $check)) {
				return true;
			}
			return false;
		}


		function validaCep ( $campo ) {
			if( !eregi("^[0-9]{5}-[0-9]{3}",$campo))	return false;
			
			return true;
		}

	
	
		function date_add($date,$num_days){
			$data = explode('/',$date); # $date deve estar no formato d/m/Y
			return date("d/m/Y", mktime(0,0,0,$data[1],$data[0],$data[2])+(86400*$num_days));
		}
		
		
		
		# formato da data YYYY-MM-DD
		function date_diff($from, $to, $sep='-'){
			#echo $from.' - '.$to;
			list($from_year, $from_month, $from_day)   	= explode($sep, $from); 
			list($to_year, $to_month, $to_day) 		 	= explode($sep, $to); 
					 
			$from_date = mktime(0,0,0,$from_month,$from_day,$from_year); 
			$to_date = mktime(0,0,0,$to_month,$to_day,$to_year);
			
			return ($to_date-$from_date); 
		}
		
		
		function datediff($interval='', $datefrom, $dateto, $using_timestamps = false) {
		/*
			$interval can be:
			yyyy - Number of full years
			q - Number of full quarters
			m - Number of full months
			y - Difference between day numbers (eg 1st Jan 2004 is "1", the first day. 2nd Feb 2003 is "33". The datediff is "-32".)
			d - Number of full days
			w - Number of full weekdays
			ww - Number of full weeks
			h - Number of full hours
			n - Number of full minutes
			s - Number of full seconds (default)
		*/
		
			if (!$using_timestamps) {
				$datefrom = strtotime($datefrom, 0);
				$dateto = strtotime($dateto, 0);
			}
			
			$difference = $dateto - $datefrom; // Difference in seconds
			
			switch($interval) {
				case 'yyyy': // Number of full years
							$years_difference = floor($difference / 31536000);
							if (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom), date("j", $datefrom), date("Y", $datefrom)+$years_difference) > $dateto) {
								$years_difference--;
							}
							if (mktime(date("H", $dateto), date("i", $dateto), date("s", $dateto), date("n", $dateto), date("j", $dateto), date("Y", $dateto)-($years_difference+1)) > $datefrom) {
								$years_difference++;
							}
							$datediff = $years_difference;
					break;
					
				case "q": // Number of full quarters
						$quarters_difference = floor($difference / 8035200);
						while (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom)+($quarters_difference*3), date("j", $dateto), date("Y", $datefrom)) < $dateto) {
							$months_difference++;
						}
						$quarters_difference--;
						$datediff = $quarters_difference;
					break;
					
				case "m": // Number of full months
						$months_difference = floor($difference / 2678400);
						while (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom)+($months_difference), date("j", $dateto), date("Y", $datefrom)) < $dateto) {
							$months_difference++;
						}
						$months_difference--;
						$datediff = $months_difference;
					break;
					
				case 'y': // Difference between day numbers
						$datediff = date("z", $dateto) - date("z", $datefrom);
					break;
				
				case "d": // Number of full days
						$datediff = floor($difference / 86400);
				break;
				
				case "w": // Number of full weekdays
						$days_difference = floor($difference / 86400);
						$weeks_difference = floor($days_difference / 7); // Complete weeks
						$first_day = date("w", $datefrom);
						$days_remainder = floor($days_difference % 7);
						$odd_days = $first_day + $days_remainder; // Do we have a Saturday or Sunday in the remainder?
						if ($odd_days > 7) { // Sunday
							$days_remainder--;
						}
						if ($odd_days > 6) { // Saturday
							$days_remainder--;
						}
						$datediff = ($weeks_difference * 5) + $days_remainder;
					break;
					
				case "ww": // Number of full weeks
						$datediff = floor($difference / 604800);
					break;
					
				case "h": // Number of full hours
						$datediff = floor($difference / 3600);
					break;
				
				case "n": // Number of full minutes
						$datediff = floor($difference / 60);
					break;
					
				default: // Number of full seconds (default)
						$datediff = $difference;
					break;
			}
		
			return $datediff;
			
		} 
		
				
		function month_name($num_month){
			switch((int)$num_month){
				case 1:
					  return "Janeiro";
					  break;
				case 2:
					  return "Fevereiro";
					  break;
				case 3:
					  return "Mar�o";
					  break;
				case 4:
					  return "Abril";
					  break;
				case 5:
					  return "Maio";
					  break;
				case 6:
					  return "Junho";
					  break;
				case 7:
					  return "Julho";
					  break;
				case 8:
					  return "Agosto";
					  break;
				case 9:
					  return "Setembro";
					  break;
				case 10:
					  return "Outubro";
					  break;
				case 11:
					  return "Novembro";
					  break;
				case 12:
					  return "Dezembro";
					  break;
			}
		}
	

	function upload($arquivo, $novolocal, $ext="JPG|JPEG|GIF") {
		// Arquivo - TMP NAME 	 |	 LOCAL PRA ONDE VAI    | 	EXTENCOES PERMITIDAS
		# $arquivo["tmp_name"] ARQUIVO NO SERVIDOR !
		#echo $novolocal;
		#exit;
		$extensao_arquivo  = $arquivo["name"];
		$a = explode('.', $extensao_arquivo);
		$b = end($a);
		
		$extensao_arquivo  = strtolower($b); 
		
		
		// RETORNANDO EXTENSOES PERMITIDAS
		$exts   = explode("|",$ext);
		$valida = false;
		
		for($i=0; $i < count($exts); $i++ ) {
			$extensao = strtolower($exts[$i]); 			// EXTENS�O PERMITIDA
			if ( $extensao_arquivo == $extensao ) {
				$valida = true;
			}
			
		}
		
		
		
		// 
		if ( $valida == false ) {
			$this->alerta_erro('Atencao!\nFormato de arquivo invalido!',1);
		}else{
			// UPLOAD DO ARQUIVO
			#$loc =  eregi_replace("classes","docs\pdfs",dirname(__FILE__));
			move_uploaded_file($arquivo["tmp_name"],$novolocal) or die();
			return true;
		}
		
	}
	
	function uploadDoc($arquivo, $novolocal, $nome, $ext="JPG|JPEG|GIF|DOC|DOCX|PDF|TXT|RTF|XLS") {
		// Arquivo - TMP NAME 	 |	 LOCAL PRA ONDE VAI    | 	EXTENCOES PERMITIDAS
		# $arquivo["tmp_name"] ARQUIVO NO SERVIDOR !

		$extensao_arquivo  = $arquivo["name"];
		$a = explode('.', $extensao_arquivo);
		$b = end($a);
		
		$extensao_arquivo  = strtolower($b); 
		
		
		// RETORNANDO EXTENSOES PERMITIDAS
		$exts   = explode("|",$ext);
		$valida = false;
		
		for($i=0; $i < count($exts); $i++ ) {
			$extensao = strtolower($exts[$i]); 			// EXTENS�O PERMITIDA
			if ( $extensao_arquivo == $extensao ) {
				$valida =  $extensao;
			}
			
		}
		
		
		
		// 
		if ( $valida == false ) {
			return false;
		}else{
			// UPLOAD DO ARQUIVO
			$ok = move_uploaded_file($arquivo["tmp_name"],$novolocal . $nome.'.'.$valida);
			if($ok){
				return $nome.'.'.$valida;
			}else{
				return false;
			}
		}
		
	}
	
	
	// FUN��O PARA VERIFICAR SE EXISTE ARQUIVO //
	function existeArquivo ( $caminho ) {
		if ( file_exists($caminho) == 1 ) {
			return true;
		}else{
			return false;
		}
	}	
	
	
	// Tratando valores //
	function clean($valor){
   		return addslashes($valor);
	}
	
	

	// Encriptando valores ///////////////////////////////////////////////////////
	function encripta ( $valor ) {
		return md5("K@*XIOzo492;z.".$valor."$%#!#&%$#");
	}	



	# FUNCOES DE DATA	   ///////////////////////////////////////////////////////
	function dataMySql2Php ( $data, $formatador='/', $exibeAno=true ) {
		if($data=='')
			return "";
		$data = explode("-",$data);
		
		if($exibeAno){
			return @$data[2] . $formatador . @$data[1] . $formatador . @$data[0];
		}else{
			return @$data[2] . $formatador . @$data[1];
		}
	}
	
	
	
	function dataPhp2MySql ( $data, $sep='-' ) {
		#echo $data . "<br>"; #exit;
		return substr($data,6,4) . $sep . substr($data,3,2) . $sep . substr($data,0,2);
	}


	function validaData ($data) {	
		if ( preg_match("/^((0[1-9]|[12]\d)\/(0[1-9]|1[0-2])|30\/(0[13-9]|1[0-2])|31\/(0[13578]|1[02]))\/\d{4}$/", $data) == 1 ) {
			return true;
		}else{
			return false;
		}
	}
	
	

	// FUNCOES DE EMAIL //////////////////////////////////////////////////////////

	function validaemail($email=""){ 
		if (eregi("^[a-z0-9\._-]+@+[a-z0-9\._-]+\.+[a-z]{2,3}$", $email)) { 
			$valida = "1"; 
		} 
		else { 
			$valida = "0"; 
		} 
		return $valida; 
	} 



	function enviaHTML ( $destino, $remetente, $assunto, $mensagem ) {

		$formato = "\nContent-type: text/html\n";
		@mail("$destino","$assunto","$mensagem","from: ".$remetente.$formato); 
		#echo 'Email Enviado para '. "$destino <br><br>Assunto<br>$assunto<br><br>Mensagem<br>$mensagem<br><br><hr><br><Br>";
	
	}
	

	
	
	/// ALERTAS E ERROS / ///////////////////////////////////////////////////////////
	// M�todo para exibir mensagem de erro //
	function alerta_erro ( $msgErro, $nPagBack ) {
		?>
		<script language="javascript"><!--
			alert('<?=$msgErro?>');
			window.history.back(-<?=$nPagBack?>);
		--></script>
		<?
		exit;
	}
	
	
	// M�todo para exibir mensagem de erro //
	function alerta_erro2 ( $msgErro, $nPagBack ) {
		?>
		<script language="javascript"><!--
			alert('<?=$msgErro?>');
			parent.location='<?=$nPagBack?>';
		--></script>
		<?
		exit;
	}





	// M�todo para voltar paginas  //
	function voltar ( $pags ) {
		?>
		<script language="javascript"><!--
			window.history.back(-<?=$pags?>);
		--></script>
		<?
	}

	// M�todo para exibir mensagem de alerta //
	function voltar2 ( $pags ) {
		?>
		<script language="javascript"><!--
			parent.location='<?=$pags?>';
		--></script>
		<?
	}


	//      M�todo para criar miniaturas & Redimensionar a imagem      //
	function redimensionaImagem ( $imagem, $thumbnail, $x, $y ) {
		  $var = false;
		  $var = fread(fopen($imagem,'r'),filesize($imagem));
		  $img = substr($var,0,3);
		  if ( $img == "GIF" ) {
				$var  = $this-> rm_gif( $imagem, $thumbnail, $x, $y);
		  }else{
				$var  = $this-> rm_jpg( $imagem, $thumbnail, $x, $y);
		  }
		  return $var;
	}






   function rm_gif ( $imagem, $thumbnail, $x, $y ) {
			 
				 $img_origem = imagecreatefromgif($imagem);
      		// PEGA AS DIMENS�ES DA IMAGEM DE ORIGEM
				 $origem_x = imagesx($img_origem); // Largura
				 $origem_y = imagesy($img_origem); // Altura
			
		 	    // ESCOLHE A LARGURA MAIOR E, BASEADO NELA, GERA A LARGURA MENOR
				 	 if($origem_x > $origem_y) { // Se a largura for maior que a altura
					 $final_x = $x; // A largura ser� a do thumbnail
					 $final_y = floor($x * $origem_y / $origem_x); // A altura � calculada
					 
					 while ( $y < $final_y ) {
					 	$final_x--;
						$final_y = floor($final_x * $origem_y / $origem_x);
						
					 }

					 $f_x = 0; // Colar no x = 0
					 $f_y = round(($y / 2) - ($final_y / 2)); // Centralizar a imagem no meio y do thumbnail
				 } else { // Se a altura for maior ou igual � largura
					 $final_y = $y;
					  while ( $x < $final_x ) {
					 	$final_y--;
						$final_x = floor($final_y * $origem_x / $origem_y);
				 }
				 }
				 // CRIA A IMAGEM FINAL PARA O THUMBNAIL
				 $img_final = imagecreatetruecolor($final_x,$final_y);

				 // COPIA A IMAGEM ORIGINAL PARA DENTRO DO THUMBNAIL
				 imagecopyresized($img_final, $img_origem, 0, 0, 0, 0, $final_x, $final_y, $origem_x, $origem_y);
			
				 // SALVA O THUMBNAIL
				 imagegif($img_final, $thumbnail,100);
			
				 // LIBERA A MEM�RIA
				 imagedestroy($img_origem);
				 imagedestroy($img_final);

				 return true;
	}
	
	
	 function rm_jpg ( $imagem, $thumbnail, $x, $y ) {
			 
			 $img_origem = imagecreatefromjpeg($imagem);
			
				// PEGA AS DIMENS�ES DA IMAGEM DE ORIGEM
				 $origem_x = imagesx($img_origem); // Largura
				 $origem_y = imagesy($img_origem); // Altura
			
				// ESCOLHE A LARGURA MAIOR E, BASEADO NELA, GERA A LARGURA MENOR
				 if($origem_x > $origem_y) { // Se a largura for maior que a altura
					 $final_x = $x; // A largura ser� a do thumbnail
					 $final_y = floor($x * $origem_y / $origem_x); // A altura � calculada
					 
					 while ( $y < $final_y ) {
					 	$final_x--;
						$final_y = floor($final_x * $origem_y / $origem_x);
						
					 }

					 $f_x = 0; // Colar no x = 0
					 $f_y = round(($y / 2) - ($final_y / 2)); // Centralizar a imagem no meio y do thumbnail
				 } else { // Se a altura for maior ou igual � largura
					 $final_y = $y; // A largura ser� a do thumbnail
					 $final_x = floor($y * $origem_x / $origem_y); // A altura � calculada
					 
					 while ( $x < $final_x ) {
					 	$final_y--;
						$final_x = floor($final_y * $origem_x / $origem_y);
						
					 }

					 $f_y = 0; // Colar no x = 0
					 $f_x = round(($x / 2) - ($final_x / 2)); // Centralizar a imagem no meio y do thumbnail
				 }
			
				// CRIA A IMAGEM FINAL PARA O THUMBNAIL
				 $img_final = imagecreatetruecolor($final_x,$final_y);
			
				// COPIA A IMAGEM ORIGINAL PARA DENTRO DO THUMBNAIL
				 imagecopyresampled($img_final, $img_origem, 0, 0, 0, 0, $final_x, $final_y, $origem_x, $origem_y);
			
				// SALVA O THUMBNAIL
				 imagejpeg($img_final, $thumbnail,100);
			
				// LIBERA A MEM�RIA
				 imagedestroy($img_origem);
				 imagedestroy($img_final);
			

			  return true;
	}
	
	
	
	
	function validaCpf ($cpf) {
		
		$nulos = array("12345678909","11111111111","22222222222","33333333333",
					   "44444444444","55555555555","66666666666","77777777777",
					   "88888888888","99999999999","00000000000");
		/* Retira todos os caracteres que nao sejam 0-9 */
		$cpf = ereg_replace("[^0-9]", "", $cpf);
		
		/*Retorna falso se houver letras no cpf */
		if ( !(ereg("[0-9]",$cpf)) ) 			return 0;
		
		/* Retorna falso se o cpf for nulo */
		if( in_array($cpf, $nulos) )
			return 0;
		
		/*Calcula o pen�ltimo d�gito verificador*/
		$acum=0;
		for($i=0; $i<9; $i++) {
		  $acum+= $cpf[$i]*(10-$i);
		}
		
		$x= $acum % 11;
		$acum = ($x>1) ? (11 - $x) : 0;
		/* Retorna falso se o digito calculado eh diferente do passado na string */
		if ($acum != $cpf[9]){
		  return 0;
		}
		/*Calcula o �ltimo d�gito verificador*/
		$acum=0;
		for ($i=0; $i<10; $i++){
		  $acum+= $cpf[$i]*(11-$i);
		}   
		
		$x= $acum % 11;
		$acum = ($x > 1) ? (11-$x) : 0;
		/* Retorna falso se o digito calculado eh diferente do passado na string */
		if ( $acum != $cpf[10]){
		  return 0;
		}   
		/* Retorna verdadeiro se o cpf eh valido */
		return 1;
	}




	function alerta($mensagem, $url_retorno='n'){
		?>
		<script language="javascript">
			alert('<?=$mensagem?>');
			<? if($url_retorno!='n'){ ?>
			location.href='<?=$url_retorno?>';
			<? } ?>
        </script>
		<?
	}


	function redir($url_retorno){
		?>
		<script language="javascript">
			location.href='<?=$url_retorno?>';
        </script>
		<?
	}




	function dblog($id_usr='',$str_log=''){
		return $this->objDao->insert("log","id_usr,descricao,data",array($id_usr,$str_log,date("Y/m/d")));
	}
	
	
	function cleanCpfCnpj($strNumero){
		$strNumero = str_replace('-','',$strNumero);
		$strNumero = str_replace('/','',$strNumero);
		$strNumero = str_replace('.','',$strNumero);
		return $strNumero;
	}
	
	
	# escrevendo valor por extenso
	function extenso( $valor, $moedaSing, $moedaPlur, $centSing, $centPlur ) {
	
	   $centenas = array( 0,
		   array(0, "cento",        "cem"),
		   array(0, "duzentos",     "duzentos"),
		   array(0, "trezentos",    "trezentos"),
		   array(0, "quatrocentos", "quatrocentos"),
		   array(0, "quinhentos",   "quinhentos"),
		   array(0, "seiscentos",   "seiscentos"),
		   array(0, "setecentos",   "setecentos"),
		   array(0, "oitocentos",   "oitocentos"),
		   array(0, "novecentos",   "novecentos") ) ;
	
	   $dezenas = array( 0,
				"dez",
				"vinte",
				"trinta",
				"quarenta",
				"cinq�enta",
				"sessenta",
				"setenta",
				"oitenta",
				"noventa" ) ;
	
	   $unidades = array( 0,
				"um",
				"dois",
				"tr�s",
				"quatro",
				"cinco",
				"seis",
				"sete",
				"oito",
				"nove" ) ;
	
	   $excecoes = array( 0,
				"onze",
				"doze",
				"treze",
				"quatorze",
				"quinze",
				"dezeseis",
				"dezesete",
				"dezoito",
				"dezenove" ) ;
	
	   $extensoes = array( 0,
		   array(0, "",       ""),
		   array(0, "mil",    "mil"),
		   array(0, "milh�o", "milh�es"),
		   array(0, "bilh�o", "bilh�es"),
		   array(0, "trilh�o","trilh�es") ) ;
	
	   $valorForm = trim( number_format($valor,2,".",",") ) ;
	
	   $inicio    = 0 ;
	
	   if ( $valor <= 0 ) {
		  return ( $valorExt ) ;
	   }
	
	   for ( $conta = 0; $conta <= strlen($valorForm)-1; $conta++ ) {
		  if ( strstr(",.",substr($valorForm, $conta, 1)) ) {
			 $partes[] = str_pad(substr($valorForm, $inicio, $conta-$inicio),3," ",STR_PAD_LEFT) ;
			 if ( substr($valorForm, $conta, 1 ) == "." ) {
				break ;
			 }
			 $inicio = $conta + 1 ;
		  }
	   }
	
	   $centavos = substr($valorForm, strlen($valorForm)-2, 2) ;
	
	   if ( !( count($partes) == 1 and intval($partes[0]) == 0 ) ) {
		  for ( $conta=0; $conta <= count($partes)-1; $conta++ ) {
	
			 $centena = intval(substr($partes[$conta], 0, 1)) ;
			 $dezena  = intval(substr($partes[$conta], 1, 1)) ;
			 $unidade = intval(substr($partes[$conta], 2, 1)) ;
	
			 if ( $centena > 0 ) {
	
				$valorExt .= $centenas[$centena][($dezena+$unidade>0 ? 1 : 2)] . ( $dezena+$unidade>0 ? " e " : "" ) ;
			 }
	
			 if ( $dezena > 0 ) {
				if ( $dezena>1 ) {
				   $valorExt .= $dezenas[$dezena] . ( $unidade>0 ? " e " : "" ) ;
	
				} elseif ( $dezena == 1 and $unidade == 0 ) {
				   $valorExt .= $dezenas[$dezena] ;
	
				} else {
				   $valorExt .= $excecoes[$unidade] ;
				}
	
			 }
	
			 if ( $unidade > 0 and $dezena != 1 ) {
				$valorExt .= $unidades[$unidade] ;
			 }
	
			 if ( intval($partes[$conta]) > 0 ) {
				$valorExt .= " " . $extensoes[(count($partes)-1)-$conta+1][(intval($partes[$conta])>1 ? 2 : 1)] ;
			 }
	
			 if ( (count($partes)-1) > $conta and intval($partes[$conta])>0 ) {
				$conta3 = 0 ;
				for ( $conta2 = $conta+1; $conta2 <= count($partes)-1; $conta2++ ) {
				   $conta3 += (intval($partes[$conta2])>0 ? 1 : 0) ;
				}
	
				if ( $conta3 == 1 and intval($centavos) == 0 ) {
				   $valorExt .= " e " ;
				} elseif ( $conta3>=1 ) {
				   $valorExt .= ", " ;
				}
			 }
	
		  }
	
		  if ( count($partes) == 1 and intval($partes[0]) == 1 ) {
			 $valorExt .= $moedaSing ;
	
		  } elseif ( count($partes)>=3 and ((intval($partes[count($partes)-1]) + intval($partes[count($partes)-2]))==0) ) {
			 $valorExt .= " de " + $moedaPlur ;
	
		  } else {
			 $valorExt = trim($valorExt) . " " . $moedaPlur ;
		  }
	
	   }
	
	   if ( intval($centavos) > 0 ) {
	
		  $valorExt .= (!empty($valorExt) ? " e " : "") ;
	
		  $dezena  = intval(substr($centavos, 0, 1)) ;
		  $unidade = intval(substr($centavos, 1, 1)) ;
	
		  if ( $dezena > 0 ) {
			 if ( $dezena>1 ) {
				$valorExt .= $dezenas[$dezena] . ( $unidade>0 ? " e " : "" ) ;
	
			 } elseif ( $dezena == 1 and $unidade == 0 ) {
				$valorExt .= $dezenas[$dezena] ;
	
			 } else {
				$valorExt .= $excecoes[$unidade] ;
			 }
	
		  }
	
		  if ( $unidade > 0 and $dezena != 1 ) {
			 $valorExt .= $unidades[$unidade] ;
		  }
	
		  $valorExt .= " " . ( intval($centavos)>1 ? $centPlur : $centSing ) ;
	
	   }
	
	   return ( $valorExt ) ;
	
	}

}



?>
