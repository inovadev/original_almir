<?
class usuario extends Conf {
	
	#atributos de configuração da classe
	var $nome_chave_primaria 	= "id_usuario";
	var $nome_tabela 			= "usuario";
	var $nome_session 			= "cadUsuario";
	var $str_campos_busca		= "nome, empresa, regional, filial";
	var $titulo_cadastro		= " USUÁRIOS ";
	
	#atributos da classe
	var $id;
	var $nome;
	var $email;
	var $login;
	var $senha;
	var $acessoAoSistema = array("Bloqueado","Liberado");
	var $sys;
	var $objDao;

	
	var $nivelUsuario = array("Matriz","Regional","Filial");
	
	
	#metodo construtor
	function usuario()
	{
		parent::conf();
		//$this->sys 			= new vision();
		$this->objDao 		= new Mysql ();
	}
	
	function pesquisar ( $str_busca, $ordem )
	{
		$this->lista["search"] 	= $this->objDao->search($this->nome_tabelaView, $this->str_campos_busca, $str_busca, $ordem);
		$this->total_paginas 	= ceil( count($this->lista["search"]) / $this->limite);
		return $this->lista["search"];
	}	
	
	function verifyLogin($valor)
	{
		$this->login = $valor['usuario']==''?parent::alerta_erro('Erro, login nao foi informado!',1):addslashes(parent::clean($valor["usuario"]));
		$this->senha = $valor['senha']==''?parent::alerta_erro('Erro, senha nao foi informado!',1):addslashes(parent::clean($valor["senha"]));
	}
	
	
	function checkNivelUser($id)
	{
		#verifica se o usuario é da matriz
		$sql = "select id_usuario from usuario where id_usuario ='$id' and not id_empresa is null and id_regional is null and id_filial is null ";
		$obj = new Mysql();
		if($obj->num_rows($sql)==1)
		{
			$x = 0; 	
		}
		$obj = NULL;
		
		#verifica se o usuario é de uma reginal
		$sql = "select id_usuario from usuario where id_usuario ='$id' and not id_empresa is null and not id_regional is null and id_filial is null ";
		$obj = new Mysql();
		if($obj->num_rows($sql)==1)
		{
			$x =  1; 	
		}
		$obj = NULL;
		
		#verifica se o usuario é de uma filial
		$sql = "select id_usuario from usuario where id_usuario ='$id' and not id_empresa is null and not id_regional is null and not id_filial is null ";
		$obj = new Mysql();
		if($obj->num_rows($sql)==1)
		{
			$x =  2; 	
		}
		$obj = NULL;
		
		return $x;
		
	}
	
	function logUser  ( $valor )
	{
		$this->verifyLogin($valor);
		//parent::encripta($this->senha)
		$sql = "select * from usuario where login = '".$this->login."' and senha = '" . $this->senha . "'";
		
		///echo $this->objDao->num_rows($sql);
		if ( $this->objDao->num_rows($sql) == 0 )
		{
			parent::alerta_erro('Login ou senha invalidos',1);
			return false;
		}
		else
		{
			$linha = $this->objDao->select($sql);
			//echo "login: ".$linha[0]["login"].'<br>';
			//echo "senha: ".$linha[0]["senha"].'<br>';
			// Loga usuario
			//$_SESSION["usuario"]["id_usuario"]		= $linha[0]["id_usuario"];
			//$_SESSION["usuario"]["nome"] 			= $linha[0]["nome"];
			$_SESSION["usuario"]["login"] 		  = $linha[0]["login"];
			$_SESSION["usuario"]["senha"]	 	    = $linha[0]["senha"];
			//$_SESSION["usuario"]["acessoAoSistema"]	= $linha[0]["acessoAoSistema"];
			//$_SESSION["usuario"]["dt_last_login"]	= $linha[0]["dt_last_login"];
			//$_SESSION["usuario"]["nivelUsuario"] 	= $this->checkNivelUser($_SESSION["usuario"]["id_usuario"]);

			#atualiza o banco informando a data do ultimo login
			//$this->objDao->update("usuario","dt_last_login",array(date("Y/m/d H:i:s")),"id_usuario = '".$linha[0]["id_usuario"]."'");
			//echo $sql;
			return true;
			
		}	
	}
	
	
/*	
	# verifica se o nivel existe
	function checkNivel($n)
	{
			if ( $this->nivelSetor[$n] == "" )
			{
				return false;
			}
			else
			{
				return true;
			}
	}
		
*/		
/*
	function nivel($n)
	{
		$var = false;
		
		for ( $x=0; $x<count($n); $x++)
		{
			if($this->checkNivel($n[$x]))
			{
				if ( (int)$_SESSION["usuario"]["setor_tipo"] == (int)$n[$x] )
				{ 
					$var = true;
					break;
				}
			}
		}
		
		if ( $var == false )
		{ 
			parent::alerta("Acao nao autorizada.\\nVerifique suas permissoes!",'javascript:history.back();');
			die();
		}
		else
		{
			return true;
		}
	
	}	
*/	

	// Verifica se usuário está registrado ou não
	function isLogged ( $usuario, $senha ) {
		//and acessoAoSistema = '1'
		$sql = "select login, senha from usuario where login = '$usuario' and senha = '$senha'";
		if ( $this->objDao->num_rows($sql) == 0 ) { 
			if($this->logOut()==false){
				//echo $sql;
				parent::alerta('Sessao expirada\\nEfetue login novamente!',$this->sys->vision_virtual_path."index.php"); 
				
				
				return false;
				die();
			}
		}else{
			return true;
		}
	}
	
	function logOut ( ) {
		$_SESSION["usuario"] = '';
		session_unregister($_SESSION["usuario"]);
		unset($_SESSION["usuario"]);
		if ( @session_unregister($_SESSION["usuario"]) ) {
			return true;
		}else{
			return false;
		}
	}

	
/*	
	
	
	function verify ( $valor ) {
		
		$this->id_empresa			= $valor["id_empresa"]==''	 	? parent::alerta_erro('Erro, empresa nao foi informado!',1)		: parent::clean($valor["id_empresa"]);
		$this->id_regional			= $valor["id_regional"]=='' 	? 'NULL' : parent::clean($valor["id_regional"]);
		$this->id_filial			= $valor["id_filial"]=='' 		? 'NULL' : parent::clean($valor["id_filial"]);
		
		if($this->id_filial!='' && $this->id_regional == ''){parent::alerta_erro('Ao selecionar uma filial, deve-se selecionar uma regional!',1);}
		
		$this->nome					= $valor["nome"]==''	 		? parent::alerta_erro('Erro, nome nao foi informado!',1)			: parent::clean($valor["nome"]);
		$this->email				= $valor["email"]==''	 		? parent::alerta_erro('Erro, email nao foi informado!',1)			: parent::clean($valor["email"]);
		$this->ramal				= parent::clean($valor["ramal"]);
		$this->id_grupoComissao		= parent::clean($valor["id_grupoComissao"]);
		$this->acessoAoSistema		= $valor["acessoAoSistema"]==''	? parent::alerta_erro('Erro, acessoAoSistema nao foi informado!',1)	: parent::clean($valor["acessoAoSistema"]);
		$this->fl_responsavel_por_dpto = parent::clean($valor["fl_responsavel_por_dpto"]);
		$this->id_perfil 			= $valor["id_perfil"]=='' ? 'NULL' : parent::clean($valor['id_perfil']);
	}
	
	
	// metodo para adicionar novos registros no banco de dados
	function inserir($valor) {
	   
		@$_SESSION[$this->nome_session] = $valor;
		
		$this->verify($valor);
		
		$this->login = $valor["login"]=='' ? parent::alerta_erro('Erro, login nao foi informado!',1) : parent::clean($valor["login"]);
		
		$this->login = $this->checaLoginExistente($valor)!=false ? parent::alerta_erro('Erro, o login informado nao esta disponivel para cadastro.\n\nTente um outro login!',1) : $this->login;
		
		$this->senha = $valor["senha"]=='' ? parent::alerta_erro('Erro, senha nao foi informado!',1) : parent::encripta($valor["senha"]);
		
		# Inserindo registro
		$tmp = $this->objDao->insert($this->nome_tabela,"id_empresa, id_regional, id_filial, id_perfil, nome, email, ramal, login, senha, id_grupoComissao, acessoAoSistema, dt_last_login, fl_responsavel_por_dpto ",
												array( $this->id_empresa, $this->id_regional, $this->id_filial, $this->id_perfil, $this->nome, $this->email, $this->ramal, $this->login, $this->senha, $this->id_grupoComissao, $this->acessoAoSistema, $this->dt_last_login, $this->fl_responsavel_por_dpto));
		
		$this->id = mysql_insert_id();
		
		if ( $tmp == true ) {
		  @$_SESSION[$this->nome_session] = '';		   
		  return true;
		}
	}


	#metodo para alterar registros no banco de dados
	function atualizar($valor) {

		$this->id = $valor[$this->nome_chave_primaria]==""?parent::alerta_erro('Erro, codigo nao foi informado!',1):parent::clean($valor[$this->nome_chave_primaria]);
   	    $this->verify($valor);


		// atualizando o registro	
		$sql = $this->objDao->update($this->nome_tabela,"id_empresa, id_regional, id_filial, id_perfil, nome, email, ramal, id_grupoComissao, acessoAoSistema, dt_last_login, fl_responsavel_por_dpto",
												array( $this->id_empresa, $this->id_regional, $this->id_filial, $this->id_perfil, $this->nome, $this->email, $this->ramal, $this->id_grupoComissao, $this->acessoAoSistema, $this->dt_last_login, $this->fl_responsavel_por_dpto),
													  "{$this->nome_chave_primaria} = '$this->id'");
		
		if ( $sql) {
			return true;
		}else{
			return false;
		}
		
	}
	
	
	# metodo para verificar se o login cadastrado já existe
	function checaLoginExistente($valor){
	
		# verifica se o login digitado ja esta cadastrado no banco para outro usuario
		$login = addslashes($valor['login']);
	
		if($login=='' ){ 
			parent::alerta_erro('Erro, login nao informado!',1); 
		} else {
			$sql = "select id_usuario, login from usuario where login = '".$login."'";

			$obj = new PostGres();
			$users = $obj->select($sql);
			if(count($users)>0){
				return $users[0]['id_usuario'];  # login ja cadastrado
			}else{
				return false; # login não encontrado na base de dados
			}
		}
	}
	
	
	function alterarLogin($valor){
	
		#verifica se o login digitado ja esta cadastrado no banco para outro usuario
		
		#login que sera cadastrado no banco e ID do funcionario que tera o login alterado
		$login 		= addslashes($valor['login']);
		$id_usuario = addslashes($valor['id_usuario']);
		
		if($login=='' || $id_usuario==''){ 
			parent::alerta_erro('Erro, login e/ou cod usuario nao informado!',1); 
		} else {
			
			$sql = "select id_usuario, login, senha from usuario where login = '".$login."'";

			$obj = new PostGres();
			$users = $obj->select($sql);
			
			# O login ja esta cadastrado no banco.
			# Agora verifica se o login é o mesmo login do usuario que está sendo alterado
			if(count($users)>0){
				
				if((string)$id_usuario==(string)$users[0]['id_usuario']){
					# informa ao usuario que ele está informando o mesmo login que o login que ja está cadastrado na base de dados
					parent::alerta('Erro, esse e o seu login atual!',"?$this->nome_chave_primaria=$id_usuario");	
					return false;
				}else{
					#Retorna erro informando que o login ja está cadastrado no banco de dados
					parent::alerta('Erro, o login solicitado ja esta cadastrado no banco de dados!',"?$this->nome_chave_primaria=$id_usuario");	
					return false;
				}
				
			}else{
				#verifica se o usuario está alterando a propria senha
				if($id_usuario==$_SESSION['usuario']['id_usuario']){
					# atualiza a session do usuario. 
					# altera o login antigo pelo solicitado
					$_SESSION['usuario']['login'] = $login;
				}
				#seta novo login ao usuario
				return $obj->update($this->nome_tabela,"login",array($login),"$this->nome_chave_primaria = '$id_usuario'");
			}
		
		}

	}
	
	
	
	
	function alterarSenha($valor){
		$senha 			= addslashes($valor['senha']);
		$id_usuario 	= addslashes($valor['id_usuario']);
		
		if($senha=='' || $id_usuario==''){ 
			parent::alerta_erro('Erro, senha e/ou cod usuario nao informado!',1); 
		}else{		
			$obj = new PostGres();
			if($id_usuario==$_SESSION['funcionario']['id_usuario']){$_SESSION['usuario']['senha'] = parent::encripta(parent::clean($senha));}
			return $obj->update($this->nome_tabela,"senha",array(parent::encripta(parent::clean($senha))),"$this->nome_chave_primaria = '$id_usuario'");
		}
	}

	
	
###################################################################################################################

	
	# Listando dados de noticias //
	function lista_registros ( $ordem='n', $cod='all', $id_empresa='n', $id_regional='n', $id_filial='n'){
		
		$where = " where 1 ";
		
		if($ordem=='n'){
			$where .= " {$this->nome_chave_primaria} desc ";
		}

		if($id_empresa!='n'){
			$where .= " and id_empresa = '$id_empresa' ";
		}
		
		if($id_regional!='n'){
			$where .= " and id_regional = '$id_regional' ";
		}
		
		if($id_filial!='n'){
			$where .= " and id_filial = '$id_filial' ";
		}
		
		if ( $cod != 'all' ) { 
			$where .= "and {$this->nome_chave_primaria} = '$cod' ";
			$sql =  "select * from {$this->nome_tabela} $where ";
			
			if ( $this->objDao->num_rows($sql) == 0 ) { return false; }
		
		}else{
			
			$sqlGeral = "select * from {$this->nome_tabela} $where order by $ordem";

			$tr = $this->objDao->num_rows($sqlGeral); 					# TR = total de registros
			$this->total_paginas = $tp = ceil( $tr / $this->limite); 	# TP = total de paginas
			
			
			
			# Link de próximo
			if ( $this->pagina < ($tp-1) ) { $this->proximo = $this->pagina+1 ;}                           
			
			$linha_inicial 	= $this->pagina*$this->limite;
			$linha_final 	= $this->limite;
			
			$busca = "select * from {$this->nome_tabela} $where order by $ordem LIMIT $linha_inicial, $linha_final";		

			$this->numRegs = $this->objDao->num_rows($busca);
			$sql = $busca;
				
		}
			 
			return  $this->objDao->select($sql);

	}

	function listaRegistrosFromView ( $ordem='n', $cod='all', $id_empresa='n', $id_regional='n',$id_filial='n'){
		
		
		$where = " where 1 ";
		
		if($ordem=='n'){
			$where .= " {$this->nome_chave_primaria} desc ";
		}
		
		if($id_empresa!='n'){
			$where .= " and id_empresa = '$id_empresa' ";
		}
		
		if($id_regional!='n'){
			$where .= " and id_regional = '$id_regional' ";
		}

		if($id_filial!='n'){
			$where .= " and id_filial = '$id_filial' ";
		}

		
		if ( $cod != 'all' ) { 
			$where .= "and {$this->nome_chave_primaria} = '$cod' ";
			$sql =  "select * from {$this->nome_tabelaView} $where ";
			
			if ( $this->objDao->num_rows($sql) == 0 ) { return false; }
		
		}else{
			
			$sqlGeral = "select * from {$this->nome_tabelaView} $where order by $ordem";

			$tr = $this->objDao->num_rows($sqlGeral); 					# TR = total de registros
			$this->total_paginas = $tp = ceil( $tr / $this->limite); 	# TP = total de paginas
			
			
			
			# Link de próximo
			if ( $this->pagina < ($tp-1) ) { $this->proximo = $this->pagina+1 ;}                           
			
			$linha_inicial 	= $this->pagina*$this->limite;
			$linha_final 	= $this->limite;
			
			$busca = "select * from {$this->nome_tabelaView} $where order by $ordem LIMIT $linha_inicial, $linha_final";		

			$this->numRegs = $this->objDao->num_rows($busca);
			$sql = $busca;
				
		}
			#echo $sql;
			return  $this->objDao->select($sql);

	}


	###################################################################################################################


	# Método para Deletar resultados
	function deletar ( $ids ) {

		for($i=0;$i<count($ids);$i++){
	    	$this->objDao->delete($this->nome_tabela,"$this->nome_chave_primaria = '{$ids[$i]}'");
		}
		
		return true;
		
	}
/**/
}
	
	
?>
