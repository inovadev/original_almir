<?
class clientes extends Conf {
	
	#atributos de configuração da classe
	var $nome_chave_primaria 	= "cod_cliente";
	var $nome_tabela 			= "cliente";
	var $titulo_cadastro		= " CLIENTES ";
	
	#atributos da classe
	var $id; #alterado pelo antonio
	var $cod_cliente;
	var $cli_nome;
	var $cli_nomeFantazia;
	var $cli_cpf;
	var $cli_cnpj;
	var $cli_rnacional;
	var $cli_ins_est;
	var $cli_ins_mun;
	var $cli_sexo;
	var $cli_contato;
	var $cli_atividade;
	var $cli_orpublico;
	var $cli_cep;
	var $cli_logadouro;
	var $cli_numero;
	var $cli_compl;
	var $cli_bair;
	var $cli_uf;
	var $cli_cid;
	var $cli_fon;
	var $cli_cel;
	var $cli_email;
	var $cli_nasc;
	var $cli_datcad;
	
	#metodo construtor
	function clientes(){
		
		parent::conf();
		$this->objDao 	= new Mysql();
		
	}
	
	function pesquisar ( $str_busca, $ordem ) {


		$this->lista["search"] = $this->objDao->search($this->nome_tabela, $this->str_campos_busca, $str_busca, $ordem);
		$this->total_paginas = ceil( count($this->lista["search"]) / $this->limite);
		return $this->lista["search"];
	}	

	
	function verify ( $valor ) {
		$this->cod_cliente			= parent::clean($valor["cod_cliente"]);
		$this->cli_nome				= $valor["cli_nome"]==''?parent::alerta_erro('Erro, nome do cliente nao foi informado!',1):parent::clean($valor["cli_nome"]);
		$this->cli_nomeFantazia		= parent::clean($valor["cli_nomeFantazia"]);
		$this->cli_cpf					= parent::clean($valor["cli_cpf"]);
		$this->cli_cnpj				= parent::clean($valor["cli_cnpj"]);
		$this->cli_rnacional			= parent::clean($valor["cli_rnacional"]);
		$this->cli_ins_est			= parent::clean($valor["cli_ins_est"]);
		$this->cli_ins_mun			= parent::clean($valor["cli_ins_mun"]);
		$this->cli_sexo				= parent::clean($valor["cli_sexo"]);
		$this->cli_contato			= parent::clean($valor["cli_contato"]);
		$this->cli_atividade			= parent::clean($valor["cli_atividade"]);
		$this->cli_orpublico			= parent::clean($valor["cli_orpublico"]);
		$this->cli_cep					= parent::clean($valor["cli_cep"]);
		$this->cli_atividade			= parent::clean($valor["cli_atividade"]);
		$this->cli_logadouro			= parent::clean($valor["cli_logadouro"]);
		$this->cli_numero				= parent::clean($valor["cli_numero"]);
		$this->cli_compl				= parent::clean($valor["cli_compl"]);
		$this->cli_bair				= parent::clean($valor["cli_bair"]);
		$this->cli_uf					= parent::clean($valor["cli_uf"]);
		$this->cli_cid					= parent::clean($valor["cli_cid"]);
		$this->cli_fon					= parent::clean($valor["cli_fon"]);
		$this->cli_cel					= parent::clean($valor["cli_cel"]);
		$this->cli_email				= parent::clean($valor["cli_email"]);
		$this->cli_nasc				= parent::clean($valor["cli_nasc"]);
		$this->cli_datcad				= parent::clean($valor["cli_datcad"]);
		
	}
	
	
	// metodo para adicionar novos registros no banco de dados
	function inserir($valor) {
		
		$this->verify($valor);

			# Inserindo registro
			$tmp = $this->objDao->insert($this->nome_tabela,"cli_nome,cli_nomeFantazia,cli_cpf,cli_cnpj,cli_rnacional,cli_ins_est,cli_ins_mun,cli_sexo,cli_contato,cli_atividade,cli_orpublico,cli_cep,cli_logadouro,cli_numero,cli_compl,cli_bair,cli_uf,cli_cid,cli_fon,cli_cel,cli_email,cli_nasc,cli_datcad",
													array(  $this->cli_nome, $this->cli_nomeFantazia, $this->cli_cpf, $this->cli_cnpj, $this->cli_rnacional, $this->cli_ins_est, $this->cli_ins_mun, $this->cli_sexo, $this->cli_contato, $this->cli_atividade, $this->cli_orpublico, $this->cli_cep, 
															$this->cli_logadouro, $this->cli_numero, $this->cli_compl, $this->cli_bair, $this->cli_uf, $this->cli_cid, $this->cli_fon, $this->cli_cel, $this->cli_email, $this->cli_nasc,$this->cli_datcad ));

			$this->id = mysql_insert_id();
			
			if ( $tmp == true ) {
			  return true;
			}

	}


	#metodo para alterar registros no banco de dados
	function atualizar($valor) {
		
		$this->id = $valor[$this->nome_chave_primaria]==""?parent::alerta_erro('Erro, codigo nao foi informado!',1):parent::clean($valor[$this->nome_chave_primaria]);
   	    $this->verify($valor);
		
		/*
		# INTEGRAÇÃO COM O AG
		# status da integracao com o ag
		if($this->AG_STATUS_INTEGRACAO==1){
			
			$sys 					= new vision();
			$fretamento_idCliente 	= $sys->print_value('idCliente_AG','cliente'," {$this->nome_chave_primaria} = '$this->id' ");
			$AG_idCliente 			= $this->agws->execCommand('getClienteWithCPFCNPJ',parent::cleanCpfCnpj($this->CNPJ));
			
			# caso o cliente ja tenha sido cadastrado no sistema de fretamento, mas ainda não tenha sido cadastrado no ag, insere o cliente no ag
			if($AG_idCliente==''){
				$metodo = 'IncluiCliente';
			}else{
				$metodo = 'AlteraCliente';	
			}
			
			$this->idCliente_AG = $this->AG_TCliente($valor,$metodo);
		}
		
		# FIM DA INTEGRAÇÃO COM O AG
		/*/
		
		

		// atualizando o registro	
		$sql = $this->objDao->update($this->nome_tabela,"AG_Grupo, AG_agenteCobrador, AG_receita, idCliente_AG, tipo, razaoSocial, nomeFantasia, responsavel, responsavel_cpf, CNPJ, inscricaoMunicipal, inscricaoEstadual, endereco, numero, cep, bairro, id_municipio, id_estado, id_pais, telefone, fax, telefoneMovel, email, site, certificacao, dt_nascimento_responsavel, restricao_bloqueado, restricao_motivo, restricao_spc, restricao_serasa, restricao_ccf, restricao_outros, financeiro_referencia, financeiro_melhor_dia, cobranca_endereco, cobranca_bairro, cobranca_pais, cobranca_uf, cobranca_municipio, cobranca_cep, retem_inss",
												array( $this->AG_Grupo, $this->AG_agenteCobrador, $this->AG_receita, $this->idCliente_AG, $this->tipo, $this->razaoSocial, $this->nomeFantasia, $this->responsavel, $this->responsavel_cpf, $this->CNPJ, $this->inscricaoMunicipal, $this->inscricaoEstadual, $this->endereco, $this->numero, $this->cep, $this->bairro, $this->id_municipio, $this->id_estado, $this->id_pais, $this->telefone, $this->fax, $this->telefoneMovel, $this->email, $this->site, $this->certificacao, $this->dt_nascimento_responsavel, $this->restricao_bloqueado, $this->restricao_motivo, $this->restricao_spc, $this->restricao_serasa, $this->restricao_ccf, $this->restricao_outros, $this->financeiro_referencia, $this->financeiro_melhor_dia, $this->cobranca_endereco, $this->cobranca_bairro, $this->cobranca_pais, $this->cobranca_uf, $this->cobranca_municipio, $this->cobranca_cep, $this->retem_inss),
													  "{$this->nome_chave_primaria} = '$this->id'");
		
		if ( $sql) {
			return true;
		}else{
			return false;
		}
		
	}

	
	
###################################################################################################################

	
	# Listando dados de noticias //
	function lista_registros ( $ordem=' id_cliente asc', $cod='all', $id_estado='n'){

		$where = " where id_empresa = '{$_SESSION['usuario']['id_empresa']}' ";
		
		if($id_estado!='n'){
			$where .= " and id_estado = '$id_estado' ";
		}
		
		if ( $cod != 'all' ) { 
			$where .= "and {$this->nome_chave_primaria} = '$cod' ";
			$sql =  "select * from {$this->nome_tabela} $where ";
			#echo $sql;
			if ( $this->objDao->num_rows($sql) == 0 ) { return false; }
		
		} else{
			
			$sqlGeral = "select * from {$this->nome_tabela} $where order by $ordem";

			$tr = $this->objDao->num_rows($sqlGeral); 					# TR = total de registros
			$this->total_paginas = $tp = ceil( $tr / $this->limite); 	# TP = total de paginas
			
			
			
			# Link de próximo
			if ( $this->pagina < ($tp-1) ) { $this->proximo = $this->pagina+1 ;}                           
			
			$linha_inicial = $this->pagina*$this->limite;
			$linha_final = $this->limite;
			
			$busca = "select * from {$this->nome_tabela} $where order by $ordem LIMIT $linha_inicial, $linha_final";		

			$this->numRegs = $this->objDao->num_rows($busca);
			$sql = $busca;
			
		}
			 
		return  $this->objDao->select($sql);

	}


	###################################################################################################################


	# Método para Deletar resultados
	function deletar ( $ids ) {

		for($i=0;$i<count($ids);$i++){
	    	$this->objDao->delete($this->nome_tabela,"$this->nome_chave_primaria = '{$ids[$i]}'");
		}
		return true;
	}

}
?>
