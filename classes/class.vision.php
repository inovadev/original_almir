<?
class vision{

	#dados do sistema
	var $vision_nome 	= "VISION!";
	var $vision_versao 	= "1.0";
	var $vision_slogan	= "Controle de Cozinha";
	
	#informações do cliente
	var $cliente 			 = "Almir Dantas";
	var $cliente_responsavel = "Almir Dantas";
	var $cliente_email 		 = "almirdant@gmail.com";
	var $cliente_site 		 = "www.ad-gastronomo.com.br";
	
	#configurações do servidor
	var $vision_phisical_path = "/";
	var $vision_virtual_path  = "/";
	
	#dados do desenvolvedor
	var $dev_autor 				= "Antonio Sales";
	var $dev_autor_site			= "http://www.naotem.com.br";
	var $dev_autor_email 		= "antoniogsnogueira@outlook.com";
	var $dev_programador 		= "Antonio Sales";
	var $dev_programador_email 	= "antoniogsnogueira@outlook.com";
	
	var $config;
	var $objDao;
	
	#metodo construtor
	function vision(){
		$this->objDao = new Mysql(); 
	}
	
	function titulo(){
		$str = $this->vision_nome ." ". $this->cliente ." - ". $this->vision_slogan; 
		return $str;
	}
	
	function cabecalho_home(){
		
		$str = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
				<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en-gb\" lang=\"en-gb\" dir=\"ltr\" id=\"minwidth\" >
				<head>
				<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
				<meta http-equiv=\"X-UA-Compatible\" content=\"IE=EmulateIE7\" />
				<title>{$this->titulo()}</title>
				<script type=\"text/javascript\" src=\"{$this->vision_virtual_path}media/system/js/mootools.js\"></script>
				<script type=\"text/javascript\">
						window.addEvent('domready', function(){ new Accordion($$('.panel h3.jpane-toggler'), $$('.panel div.jpane-slider'), {onActive: function(toggler, i) { toggler.addClass('jpane-toggler-down'); toggler.removeClass('jpane-toggler'); },onBackground: function(toggler, i) { toggler.addClass('jpane-toggler'); toggler.removeClass('jpane-toggler-down'); },duration: 500,opacity: true}); });
				</script>
				<link rel=\"stylesheet\" href=\"{$this->vision_virtual_path}templates/system/css/system.css\" type=\"text/css\" />
				<link href=\"{$this->vision_virtual_path}templates/khepri/css/template.css\" rel=\"stylesheet\" type=\"text/css\" />
				<!--[if IE 7]>
				<link href=\"{$this->vision_virtual_path}templates/khepri/css/ie7.css\" rel=\"stylesheet\" type=\"text/css\" />
				<![endif]-->
				<!--[if lte IE 6]>
				<link href=\"{$this->vision_virtual_path}templates/khepri/css/ie6.css\" rel=\"stylesheet\" type=\"text/css\" />
				<![endif]-->
				<link rel=\"stylesheet\" type=\"text/css\" href=\"{$this->vision_virtual_path}templates/khepri/css/rounded.css\" />
				<script type=\"text/javascript\" src=\"{$this->vision_virtual_path}templates/khepri/js/menu.js\"></script>
				<script type=\"text/javascript\" src=\"{$this->vision_virtual_path}templates/khepri/js/index.js\"></script>
				</head>";
		
		return $str;
	
	}
	
	function cabecalho(){
		
		?>
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" dir="ltr" id="minwidth" >
        <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
        <title><?=$this->titulo();?></title>
        <link rel="stylesheet" href="<?=$this->vision_virtual_path?>media/system/css/modal.css" type="text/css" />
        <script type="text/javascript" src="<?=$this->vision_virtual_path?>includes/js/joomla.javascript.js"></script>
        <script type="text/javascript" src="<?=$this->vision_virtual_path?>media/system/js/mootools.js"></script>
        <script type="text/javascript" src="<?=$this->vision_virtual_path?>media/system/js/modal.js"></script>
		<script type="text/javascript">
                window.addEvent('domready', function(){ new Accordion($$('.panel h3.jpane-toggler'), $$('.panel div.jpane-slider'), {onActive: function(toggler, i) { toggler.addClass('jpane-toggler-down'); toggler.removeClass('jpane-toggler'); },onBackground: function(toggler, i) { toggler.addClass('jpane-toggler'); toggler.removeClass('jpane-toggler-down'); },duration: 500,opacity: true}); });
        </script>
		<script type="text/javascript">
                window.addEvent('domready', function(){ var JTooltips = new Tips($$('.hasTip'), { maxTitleChars: 50, fixed: false}); });
                window.addEvent('domready', function() {
        
                    SqueezeBox.initialize({});
        
                    $$('a.modal').each(function(el) {
                        el.addEvent('click', function(e) {
                            new Event(e).stop();
                            SqueezeBox.fromElement(el);
                        });
                    });
                });
        </script>        
		<link rel="stylesheet" href="<?=$this->vision_virtual_path?>templates/system/css/system.css" type="text/css" />
        <link href="<?=$this->vision_virtual_path?>templates/khepri/css/template.css" rel="stylesheet" type="text/css" />
        <!--[if IE 7]>
        <link href="<?=$this->vision_virtual_path?>templates/khepri/css/ie7.css" rel="stylesheet" type="text/css" />
        <![endif]-->
        <!--[if lte IE 6]>
        <link href="<?=$this->vision_virtual_path?>templates/khepri/css/ie6.css" rel="stylesheet" type="text/css" />
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="<?=$this->vision_virtual_path?>templates/khepri/css/rounded.css" />
        <script type="text/javascript" src="<?=$this->vision_virtual_path?>templates/khepri/js/menu.js"></script>
        <script type="text/javascript" src="<?=$this->vision_virtual_path?>templates/khepri/js/index.js"></script>
        <script type="text/javascript" src="<?=$this->vision_virtual_path?>include/js/script.js"></script>
        <script type="text/javascript" src="<?=$this->vision_virtual_path?>include/js/ajax.js"></script>
        <style media="print">
        	#toolbar-box, #module-menu{
				display:none !important;	
			}
			
			*{
				font-size:9px !important;	
			}
			.adminlist{
				position:absolute;
				top:0;
				left:0;
			}
        </style>
        </head>		
        <style >
        	.inputbox{
				text-transform:uppercase !important;	
			}
        </style>
		<?
	}
	
	
	function topo($user='',$tpuser='',$DTUA=''){
		?>
        <div id="border-top" class="h_green">
          <div>
            <div> <span class="version">
            <?
            	if($_SESSION["usuario"]["regional"]!='' && $_SESSION["usuario"]["filial"]=='' ){
					echo " Usuário: ".$_SESSION["usuario"]["nome"]." - ".$_SESSION["usuario"]["regional"];	
				}
            	if($_SESSION["usuario"]["filial"]!=''){
					echo " Usuário: ".$_SESSION["usuario"]["nome"]." - ".$_SESSION["usuario"]["regional"]." » ".$_SESSION["usuario"]["filial"];	
				}
            	if($_SESSION["usuario"]["regional"]==''&&$_SESSION["usuario"]["filial"]==''){
					echo " Usuário: ".$_SESSION["usuario"]["nome"];	
				}
			?>
           </span></div>
          </div>
        </div>
        <div style="font:normal 1px tahoma;height:3px;background:#CC0000;text-indent:-5000px;">-1-1-1-1-1</div>
        <div id="header-box">
          <div id="module-status"></div>
          <? $this->menu();?>
          <div class="clr"></div>
        </div>
		<?	
	}
	
	function menu(){
	
		?>
            <div id="module-menu">
            	
                <ul id="menu" >
                <li><a>Início</a>
                    <ul>
                      <li><a href="<?=$this->vision_virtual_path?>inicial.php">Página Inicial</a></li>
                      <li class="separator"><span></span></li>
                      <li><a class="icon-16-config" href="#" onclick="window.open('<?=$this->vision_virtual_path?>cadastros/usuario/alterarSenha_usuarios.php','','width=400,height=160,scrollbars=no')">Alterar Senha</a></li>
                      <li class="separator"><span></span></li>
                      <li><a class="icon-16-logout" href="<?=$this->vision_virtual_path?>logar.php?acao=logout">Sair do sistema</a></li>
                    </ul>
                 </li>
                 <li class="node"><a>Cadastros</a>
                    <ul>
                      <li class="separator"><span></span></li>
                        <li><a href="<?=$this->vision_virtual_path?>cadastros/adm_empresa/index.php" class="icon-16-category">Empresas</a></li>
                        <li class="separator"><span></span></li>
                        <li><a href="<?=$this->vision_virtual_path?>cadastros/adm_regional/index.php" class="icon-16-category">Regionais</a></li>
                        <li class="separator"><span></span></li>
                        <li><a href="<?=$this->vision_virtual_path?>cadastros/adm_filial/index.php" class="icon-16-category">Filiais</a></li>
                        <li class="separator"><span></span></li>
                          <li class="node"><a class="icon-16-config">Manutenção Sistema AG</a>
                            <ul>
                                <?
                                $AGCAD = new AGCAD();
                                $totalCad = count($AGCAD->AGCAD);
                                
                                for($i=0;$i<$totalCad;$i++){
                                ?>
                                <li><a href="<?=$this->vision_virtual_path?>cadastros/AGCAD/index.php?AGCAD=<?=$i?>" ><?=$AGCAD->AGCAD[$i]?></a></li>
                                <li class="separator"><span></span></li>
                                <? } ?>
                            </ul>
                          </li><li class="separator"><span></span></li>
                        <li class="node"><a class="icon-16-language">Localidades</a>
                            <ul>
                                <li><a href="<?=$this->vision_virtual_path?>cadastros/pais/index.php" >País</a></li>
                                <li class="separator"><span></span></li>
                                <li><a href="<?=$this->vision_virtual_path?>cadastros/UF/index.php" >UF</a></li>
                                <li class="separator"><span></span></li>
                                <li><a href="<?=$this->vision_virtual_path?>cadastros/municipios/index.php" >Município</a></li>
                            </ul>
                        </li>
                      <li class="separator"><span></span></li>
                      <li><a href="<?=$this->vision_virtual_path?>cadastros/comissionados/index.php" class="icon-16-stats">Comissionados</a></li>
                      <li class="separator"><span></span></li>
                      <li><a href="<?=$this->vision_virtual_path?>cadastros/servico_grupo_comissao/index.php" class="icon-16-stats">Grupos de Comissões</a></li>
                      <li class="separator"><span></span></li>
                      <li><a href="<?=$this->vision_virtual_path?>cadastros/nome_tp_veiculo/index.php" class="icon-16-static">Tipo de Veículo</a></li>
                      <li class="separator"><span></span></li>
                      <li><a href="<?=$this->vision_virtual_path?>cadastros/tabela_de_preco/index.php" class="icon-16-static">Tabelas de preço</a></li>
                      <li class="separator"><span></span></li>
                      <li><a  href="<?=$this->vision_virtual_path?>cadastros/vagas/index.php" class="icon-16-static">Vagas</a></li>
                      <li class="separator"><span></span></li>
                      <li><a  href="<?=$this->vision_virtual_path?>cadastros/promotor_visita/index.php"  class="icon-16-static">Visitas</a></li>

                    </ul>
                 
                  </li>
                  <li><a href="<?=$this->vision_virtual_path?>cadastros/cliente/index.php">Clientes</a></li>
                  
                  <li><a >Serviços</a>
                    <ul>
                      <li><a href="<?=$this->vision_virtual_path?>cadastros/servicos/index.php" >Todos os serviços</a></li>
                      <li class="separator"><span></span></li>
                      <li><a href="<?=$this->vision_virtual_path?>cadastros/servicos/index.php?meusServicos=sim" >Meus serviços</a></li>
                    </ul>
                  </li>
                  
                  <li><a  href="<?=$this->vision_virtual_path?>cadastros/mensagem/index.php">Mensagens</a></li>
                  
                  <li><a>Relatórios</a>
<?php /*?>                    <ul>
                      <li class="node"><a href="<?=$this->vision_virtual_path?>reports/?id=0001" >Clientes</a>
                            <ul>
                              <li><a href="<?=$this->vision_virtual_path?>reports/?id=0001" target="_new" >Todos</a></li>
                              <li class="separator"><span></span></li>
                              <li><a href="<?=$this->vision_virtual_path?>reports/?id=0003" target="_new" >Pessoa Física</a></li>
                              <li class="separator"><span></span></li>
                              <li><a href="<?=$this->vision_virtual_path?>reports/?id=0002" target="_new" >Pessoa Jurídica</a></li>
                              <li class="separator"><span></span></li>
                            </ul>
                      </li>
                      <li class="separator"><span></span></li>
                      <li ><a href="<?=$this->vision_virtual_path?>reports/?id=0004" >Empresas</a></li>
                      <li class="separator"><span></span></li>
                      <li ><a href="<?=$this->vision_virtual_path?>reports/?id=0005" >Filiais</a></li>
                      <li class="separator"><span></span></li>
                      <li ><a href="<?=$this->vision_virtual_path?>reports/?id=0006" >Grupos de Comissões</a></li>
                      <li class="separator"><span></span></li>
                      <li ><a href="<?=$this->vision_virtual_path?>reports/?id=0007" >Tabelas de preço</a></li>
                      <li class="separator"><span></span></li>
                      <li class="node"><a>Serviços</a>
                            <ul>
                              <li><a href="<?=$this->vision_virtual_path?>reports/?id=0008" target="_new" >Por status</a></li>
                            </ul>
                      </li>
                    </ul>
<?php */?>                  </li>
                  
                  <li class="node"><a>Configurações</a>
                    <ul>
                      <li class="separator"><span></span></li>
                      <li><a href="<?=$this->vision_virtual_path?>cadastros/adm_perfil/index.php" class="icon-16-user">Perfíl de acesso</a></li>
                      <li class="separator"><span></span></li>
                      <li><a href="<?=$this->vision_virtual_path?>cadastros/usuario/index.php" class="icon-16-user">Usuários</a></li>
                      <li class="separator"><span></span></li>
                      <li><a class="icon-16-config" href="<?=$this->vision_virtual_path?>cadastros/sys_config/index.php">Configurações</a></li>
                      <li class="separator"><span></span></li>
                      <li><a class="icon-16-cpanel" href="<?=$this->vision_virtual_path?>cadastros/logs/index.php">Log</a></li>
                    </ul>
                  </li>
                 <? # FIM DO MENU "CONTROLE"?>
                  
                  
				  

                </ul>
                
                <div align="right" style="margin:6px 0 0 0; padding:0 5px 0 0;">Seu último acesso foi em:&nbsp;
				<?
				$data = explode(' ', $_SESSION["usuario"]["dt_last_login"]);
                $brData = explode('-',$data[0]);
				echo $brData[2] . '/' . $brData[1] . '/' . $brData[0] . ' ' . $data[1];
				?>
                </div>
                
              </div>
        <?
	
	}
	
	function botoesIndex(){
		?>
            <table class="toolbar">
              <tr>
                <td class="button" id="toolbar-new"><a href="#" class="toolbar" onClick="history.back();"> <span class="icon-32-back" title="Back"></span> Voltar </a></td>
                <td class="button" id="toolbar-new"><a href="editar.php" class="toolbar"> <span class="icon-32-new" title="New"> </span> Incluir </a></td>
                <td class="button" id="toolbar-new"><a href="#" class="toolbar" onClick="validarDeleteAll(document.check_list);"> <span class="icon-32-cancel" title="Close"></span> Excluir</a></td>
              </tr>
            </table>
		<?
	}
	
	
	
	function html_select($name,$tabela,$id='n',$label,$value,$where='n',$onChange='n',$campos='n',$ordem='n',$firstItem=true,$width='100%',$addStyles='n',$firstItemName='selecione...'){
		
		if($where!='n'){$where = " where 1 and ".stripslashes($where)." ";} else { $where=''; }
		
		if($onChange!='n')	{$onChange 	 = " $onChange(this.options[this.selectedIndex].value)  ";} else { $onChange=''; }
		if($campos=='n')	{$campos 	 = " $label, $value  ";}
		if($ordem=='n')		{$ordem 	 = " $label  asc";}

		$obj 	= new PostGres();
		$sql	= "select $campos from $tabela $where order by $ordem";
		#echo $sql.'<br />';
		$lista 	= $obj->select($sql);
		
		if($onChange!='n'){$onChange = " onChange=\"$onChange\" ";} else { $onChange=''; }
		
		$str = "<select name='$name' $onChange style='width:$width; $addStyles' >\n";
			
			if($firstItem==true){
				$str .= "\t<option value=''>$firstItemName</option>\n";
			}
			
			for ($i=0;$i<count($lista);$i++){
				$selected = "";
				if( $id!='n' && (string)$id==(string)$lista[$i][$value] ){
					$selected = "selected";
				}else{
					$selected = "";
				}
				$str .= "\t<option value='".$lista[$i][$value]."' title='".$lista[$i][$label]."'  $selected style='text-transform:uppercase'>".$lista[$i][$label]."</option>\n";
			}
			 
		$str .= "</select>\n"; 
		
		return $str;
		
	}
	
	
	function html_select_array($index=0,$name,$array,$selectedValue='n',$onChange='n',$firstItem=false,$width='100%'){
		
		
		if($onChange!='n'){$onChange = " $onChange(this.options[this.selectedIndex].value); ";} else { $onChange=''; }
		if($onChange!='n'){$onChange = " onChange=\"$onChange\" ";} else { $onChange=''; }
		
		$str = "<select name='$name' $onChange style='width:$width;' >\n";
			
			if($firstItem==true){
				$str .= "\t<option value=''>selecione...</option>\n";
			}
			
			for ($i=$index;$i<count($array);$i++){
				if( $selectedValue!='n' && (string)$selectedValue==(string)$i ){
					$selected = "selected";
				}else{
					$selected = "";
				}
				$str .= "\t<option value='".$i."' title='".$array[$i]."'  $selected style='text-transform:uppercase' >".$array[$i]."</option>\n";
			}
			 
		$str .= "</select>\n"; 
		
		return $str;
		
	}
	
	
	function print_value($name,$tabela,$where='n'){
		
		if($where!='n'){$where = " where 1 and $where  ";} else { $where=''; }

		$obj 	= new PostGres();
		$sql 	= "select $name from $tabela $where limit 0,1";
		#echo $sql;
		$lista 	= $obj->select($sql);
		
		return $lista[0][$name];
	}
	
	
	
	function form_busca($ordem='',$addParam=''){
	
		?>
            <form action="" method="get" name="pesquisar">
            <table>
              <tr>
                <td width="100%"> Digite uma palavra chave:
                  <input type="text" name="search" id="search" style="width:30%;"  class="text_area" />
                  <button onClick="this.form.submit();">Pesquisar</button>
                  <button onClick="this.form.reset();location.href='index.php';">Reset</button>
                  </td>
                <td nowrap="nowrap"><?=$addParam;?></td>
              </tr>
            </table>
            </form>
		<?
	
	}
	
	function ordenacao($label,$campo){
		return utf8_encode($label) . "&nbsp;&nbsp; <a href='?ordem=$campo asc'><img src='".$this->vision_virtual_path."images/M_images/sort0.png'  /></a>&nbsp;&nbsp;<a href='?ordem=$campo desc'><img src='".$this->vision_virtual_path."images/M_images/sort1.png'  /></a>";
	}
	
	function paginacao($totalPaginas,$paginaAtual,$addParam=''){
		
			$addParam = '&'.$_SERVER['QUERY_STRING'];
		
		$id = sha1(md5(sha1('ase@&bvcn$!@as*#&1saas@*(()'.session_id().'ase@&bvcn$!@as*#&1saas@*(()')));
		?>
        <del class="container">
        <div class="pagination">
          <div class="limit">Exibir #
            <select name="limit" 	id="limit" class="inputbox" size="1" onchange="location.href='?z=1<?=$addParam?>&limite='+this.options[this.selectedIndex].value;">
              <option value="5" 	<?=(string)$_SESSION['limite']=='5'   ? 'selected' : '' ;?> >5</option>
              <option value="10" 	<?=(string)$_SESSION['limite']=='10'  ? 'selected' : '' ;?> >10</option>
              <option value="15" 	<?=(string)$_SESSION['limite']=='15'  ? 'selected' : '' ;?>>15</option>
              <option value="20" 	<?=(string)$_SESSION['limite']=='20'  ? 'selected' : '' ;?>>20</option>
              <option value="25" 	<?=(string)$_SESSION['limite']=='25'  ? 'selected' : '' ;?>>25</option>
              <option value="30" 	<?=(string)$_SESSION['limite']=='30'  ? 'selected' : '' ;?>>30</option>
              <option value="50" 	<?=(string)$_SESSION['limite']=='50'  ? 'selected' : '' ;?>>50</option>
              <option value="100" 	<?=(string)$_SESSION['limite']=='100' ? 'selected' : '' ;?>>100</option>
            </select>
          </div>
          <div class="button2-right">
            <div class="start"><a href="" >início</a></div>
          </div>
          <div class="button2-right">
          	<? 
			if ( ($paginaAtual-1) > 0 ){
				$prev = "?z=1".$addParam."&pag=" . ($paginaAtual-1);
			}else{
				$prev = "?z=1".$addParam."&pag=0";
			}
			?>
            <div class="prev"><a href="<?=$prev?>" >anterior</a></div>
          </div>
          <div class="button2-left">
            <div class="page" style="background:#f3f3f3;">
            	<select onchange="window.location='?z=1<?=$addParam;?>&pag='+this.options[this.selectedIndex].value+'';">
            	<? for ($i=0;$i<(int)$totalPaginas;$i++){ ?>
            		<option value="<?=$i?>" <?=$i==$paginaAtual ? 'selected' : ''?>><?=$i+1?></option>
				<? } ?>
                </select>
                <input name="pag" type="text" style="width: 20px; margin-right:5px; text-align:center;padding:2px;" title="informe o numero da pagina" maxlength="<?=strlen((string)$totalPaginas);?>" onkeypress="if(window.event.keyCode==13){window.location='?pag='+(this.value-1)+'<?=$addParam?>';} numberOnly(); " />
            </div>
          </div>
          <div class="button2-left">
          	<? 
			if ( ($paginaAtual+1) < $totalPaginas ){
				$next = "?z=1$addParam&pag=" . ($paginaAtual+1);
			}else{
				$next = "?z=1$addParam&pag=" . ($totalPaginas-1);
			}
			?>
            <div class="next"><a href="<?=$next?>" >próximo</a></div>
            
          </div>
          <div class="button2-left">
            <div class="end"><a href="" >fim</a></div>
          </div>
          <div class="limit">Pág. <?=$paginaAtual+1;?> de <?=$totalPaginas;?></div>
          <input type="hidden" name="limitstart" value="15" />
        </div>
        </del>		
	<?
	}
	
	#funcao para inserir o rodapé do sistema
	function rodape(){
		$str = "<div id='footer'>
  				<p class='copyright'> <a href='#'>{$this->vision_nome} {$this->cliente}</a> - Sistema de Fretamento<br />
  				Powered by <a href='{$this->dev_autor_site}' target='_blank'>{$this->dev_autor}</a></p>
				</div>";
		return $str;
	}
	

}

?>
