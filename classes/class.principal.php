<?
class principal{

	#dados do sistema
	var $vision_nome 	= "GATRONOMICO!";
	var $vision_versao 	= "1.0";
	var $vision_slogan	= "Sistema Gastronômico";
	
	#metodo construtor
	function principal(){
		#$this->objDao = new MySql(); 
	}
	function titulo(){
		$str = $this->vision_nome ." ". $this->vision_slogan; 
		return $str;
	}	
	function cabecalho(){
		
		?>
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" dir="ltr" id="minwidth">
        <head>
				<link rel="shortcut icon" href="/sistema/img/favicon.png" >	
        <title><?=$this->titulo();?></title>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
				<link href="/sistema/css/style.css" rel="stylesheet" type="text/css">
				<link href="/sistema/css/style_menu.css" rel="stylesheet" type="text/css">
				
        <script type="text/javascript" src="/sistema/js/jquery.js"></script>
        <script type="text/javascript" src="/sistema/js/jquery.maskedinput.js"></script>
        
        
        </head>
		<body>
		<div id="container">
		  <div id="banner"> <img src="/sistema/img/banner.png" alt="banner" title="banner" /> </div>
		  
        <?
	}

	function menu(){
		
		?>
		<div class='cssmenu'>
			<ul>
				<li class='active '><a href='#'><span>Cadastro</span></a>
					<ul>
						<li><a href="/sistema/cadastro/cliente/cliente.php"><div style="float:left" align="left"><span>Cliente</span></div></a></li>
						<li><a href="#"><div style="float:left" align="left"><span>Estoque</span></div></a></li>
						<li><a href="/sistema/cadastro/ficha_tecnica/ficha_tecnica.php"><div style="float:left" align="left"><span>Ficha Técnica</span></div></a>
						<li><a href="/sistema/cadastro/fornecedor/fornecedor.php"><div style="float:left" align="left"><span>Fornecedor</span></div></a>
						<li><a href="/sistema/cadastro/lista_compras/list_comp.php"><div style="float:left" align="left"><span>Lista de Compras</span></div></a>
						<li><a href="#"><div style="float:left" align="left"><span>Nota Fiscal</span></div></a></li>
						<li><a href="#"><div style="float:left" align="left"><span>Produto</span></div></a></li>
						<li><a href="/sistema/cadastro/usuarios/usuarios.php"><div style="float:left" align="left"><span>Usuários</span></div></a></li>
					</ul>
				</li>
						<li>
							<a href='#'><span>Movimento</span></a>
							<ul>
							<!--	<li><a href='#'><div style="float:left" align="left"><span>Entrada Produtos</span></div></a></li>
								<li><a href='#'><div style="float:left" align="left"><span>Saída Produtos</span></div></a></li>-->
								<?#	<li><a href='#'><span>Saída Produtos</span></a></li> ?>
								<?#	<li><a href='#'><span>Saída Produtos</span></a></li> ?>
							</ul>
						</li>
						<li><a href='#'><span>Relatórios</span></a>
							<ul>
								<li><a href='#'><div style="float:left" align="left"><span>Ficha técnica</span></div></a></li>
								<li><a href='#'><div style="float:left" align="left"><span>Ficha técnica simplificada</span></div></a></li>
								<li><a href='#'><div style="float:left" align="left"><span>Lista de compras</span></div></a></li>
							</ul>
						</li>
						<li><a href="/sistema/cadastro/contatos/contato.php"><span>Contatos</span></a></li>
						<li><a href="/sistema/logar.php?acao=logout"><span>Sair</span></a></li>
						<li><a href="/sistema/home.php"><span>Principal</span></a></li>
			</ul>
		</div> 		
		
		
		<?
		
	}


	function rodape(){
	?>
	
	<div id="footer" align="center"> &copy Todos os direitos reservados para Almir Dantas</div>

	</div>	
	</body>
	</html>
	<?
	}
}

?>
