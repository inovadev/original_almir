<?php
include("../../cabecalho.php");
$sys = new principal();
$sys->cabecalho();

?>
		<script>
			$(document).ready(function(){

			$("#for_fon").mask("\(99\)9999-9999");
			$("#for_cel").mask("\(99\)9999-9999");
			$("#for_cep").mask("99999-999");
			$("#for_nasc").mask("99/99/9999");
			$("#for_datcad").mask("99/99/9999");

			//ação dos botões 
			$('#cadastra').forck(function(){
			$("#fornecedor").attr("action", "executar.php?acao=add")
			$("#fornecedor").submit();
			});	
			$('#editar').forck(function(){
			$("#fornecedor").attr("action", "executar.php?acao=edit")
			$("#fornecedor").submit();
			});	
			$('#delete').forck(function(){
			$("#fornecedor").attr("action", "")
			$("#fornecedor").submit();
			});
			$('#cancelar').forck(function(){
			$("#fornecedor").attr("action", "")
			$("#fornecedor").submit();
			});
				

			$('.ver_mask').forck(function(){
			$('input:radio[name=rg_nacional]').each(function() {

				//Verifica qual está selecionado
			if ($(this).is(':checked'))
			valor = parseInt($(this).val());

			if ( valor == 1 ){
			$("#for_rnacional").mask("999.999.999-99");
			}else{
			$("#for_rnacional").mask("99.999.999/9999-99");
			}

			});           
			});


			});
			</script>
<div id="navcontainer">
<?  $sys->menu(); ?>
</div>

<form name="fornecedor" id="fornecedor" method="post">
	<div id="content">
	<div id="formulario" align="center">

		<table border="1">
			<tr align="center"><td><p><h1>Fornecedor</h1></td></tr>
			<tr>
				<td bgcolor="#f2f2f2" width="2%">Código:
				<input type="text" name="cod_for" maxlength="4" size="2" disabled >
				Nome/Razão Social: <input type="text" name="for_nome" id="for_nome" maxlength="30" size="21">
				Nome Fantasia: <input type="text" name="for_nomeFantazia" id="for_nomeFantazia" maxlength="20" size="11">
				CPF <input type="radio" class="ver_mask" id="for_cpf" name="rg_nacional" value="1" >
				CNPJ<input type="radio" class="ver_mask" id="for_cnpj" name="rg_nacional" value="2" >
				<input type="text" id="for_rnacional" name="for_rnacional" maxlength="18" size="11">
				ID/IE: <input type="text" name="for_ins_est" id="for_ins_est" maxlength="10" size="6">
				Insc. Municipal: <input type="text" name="for_ins_mun" id="for_ins_mun" maxlength="6" size="6">
				</td>
			</tr>
			<tr>
				<td bgcolor="#f2f2f2" width="2%">Atividade: 
					<input type="text" name="for_atividade" id="for_atividade" maxlength="10" size="6">
					Orgão Público: <select name="for_orpublico" id="for_orpublico">
											<option>Selecione</option>
											<option value="mun">Municipal</option>
											<option value="est">Estadual</option>
											<option value="fed">Federal</option>
										</select>
					CEP: <input type="text" id="for_cep" name="for_cep" maxlength="10" size="6">
					Logradouro:	<input type="text" id="for_logadouro" name="for_logadouro" maxlength="35" size="26">
					Complemento:<input type="text" id="for_compl" name="for_compl" maxlength="30" size="21">
					Numero: <input type="text" id="for_numero" name="for_numero" maxlength="5" size="3">
				</td>
			</tr>	
			<tr>
				<td bgcolor="#f2f2f2" width="2%">Bairro:
				<input type="text" id="for_bair" name="for_bair" maxlength="30" size="21">
					UF: <select name="for_uf" id="forente_uf">
							 <option>Selecione</option>
							 <option value="AC">AC</option>
							 <option value="AL">AL</option>
							 <option value="AP">AP</option>
							 <option value="AM">AM</option>
							 <option value="BA">BA</option>
							 <option value="CE">CE</option>
							 <option value="ES">ES</option>
							 <option value="DF">DF</option>
							 <option value="MA">MA</option>
							 <option value="MT">MT</option>
							 <option value="MS">MS</option>
							 <option value="MG">MG</option>
							 <option value="PA">PA</option>
							 <option value="PB">PB</option>
							 <option value="PR">PR</option>
							 <option value="PE">PE</option>
							 <option value="PI">PI</option>
							 <option value="RJ">RJ</option>
							 <option value="RN">RN</option>
							 <option value="RS">RS</option>
							 <option value="RO">RO</option>
							 <option value="RR">RR</option>
							 <option value="SC">SC</option>
							 <option value="SP">SP</option>
							 <option value="SE">SE</option>
							 <option value="TO">TO</option>
						</select>	
				Cidade: <input type="text" id="for_cid" name="for_cid" maxlength="30" size="22">
				Fone: <input type="text" id="for_fon" name="for_fon" maxlength="9" size="5">
				celular: <input type="text" id="for_cel" name="for_cel" maxlength="9" size="5">
				email: <input type="text" id="for_email" name="for_email" maxlength="30" size="25">		
				</td>							
			</tr>		
			<tr>	
				<td bgcolor="#f2f2f2" width="2%">
			  	Cadastrado em: <input type="time" id="for_datcad" name="for_datcad" maxlength="10" size="4">
			  	Contato:	<input type="text" name="for_contato" id="for_contato" maxlength="20" size="11">
			  	</td>
			</tr>
		
	
	
		
			<tr>
				<td colspan="4" align="center"><br />
				<input type="button" class="button" name="novo" id="cadastra" value="Cadastrar" />
				<input type="button" class="button" name="novo" id="editar" value="Editar" />
				<input type="button" class="button" name="novo" id="delete" value="Excluir" />
				<input type="button" class="button" name="novo" id="cancelar" value="Cancelar" />
				<br><br>
				</td>
			</tr>
				
				
		</table>
	</div>
	</div>	
</form>
<?
$sys->rodape();
?>