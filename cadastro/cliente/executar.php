<? 
	include("../../cabecalho.php");
	#$checkLogin->nivel(array(1,5));
	
	$sys = new principal();
	$obj = new clientes();
	
	
	$tmp_arr = array();
	
	$acao 	 = ""; #acao para deletar arquivos
	$str_url = "";
	
	
	#inserindo novos registros
	if(strtolower($_GET['acao'])=='add'){
	

		# CONTROLE DE ACESSO DO USUARIO
		//$__perfil = new adm_perfil();
		//$__perfil->accessControl('filial_incluir');
	
	
		if($obj->inserir($_POST)){
			#$obj->dblog("0","O Usuário administrador inseriu no sistema a atividade: " . strtoupper($_POST['titulo']) );
			/*
			if(strtolower($_GET['act'])=='salvar'){
				$str_url = 'index.php';
			}elseif(strtolower($_GET['act'])=='aplicar'){
				
			}
			/**/
			$str_url = 'cliente.php?id='.$obj->id;
			$obj->alerta("Registro cadastrado com sucesso!",$str_url);
		}
	
	
	#atualizando registros
	}elseif(strtolower($_GET['acao'])=='edit'){
	
	
		# CONTROLE DE ACESSO DO USUARIO
		//$__perfil = new adm_perfil();
		//$__perfil->accessControl('filial_alterar');
	
	
		if($obj->atualizar($_POST)){
			#$obj->dblog("0","O Usuário administrador alterou no sistema a atividade de ID " . strtoupper($_POST[$obj->nome_chave_primaria]) . ' para ' . strtoupper($_POST['titulo']) );
			/*
			if(strtolower($_GET['act'])=='salvar'){
				$str_url = 'cliente.php';
			}elseif(strtolower($_GET['act'])=='aplicar'){
				$str_url = 'cliente.php';
			}
			/**/
			$str_url = 'cliente.php';
			$obj->alerta("Registro alterado com sucesso!",$str_url);
		}
	
	
	#deletando vários registros
	}elseif(strtolower($_GET['acao'])=='delete_all'){
		$acao = 'delete';
		foreach($_POST as $id => $value){
			if($id!='n'){
				array_push($tmp_arr,$id);
			}
		}
	
	
	#deletando um registro
	}elseif(strtolower($_GET['acao'])=='delete_one'){
		$acao = 'delete';
		array_push($tmp_arr,$_GET['id']);
	}
	
	if($acao=='delete'){
	
	
		# CONTROLE DE ACESSO DO USUARIO
		$__perfil = new adm_perfil();
		$__perfil->accessControl('filial_excluir');
	
	
		if($obj->deletar($tmp_arr)){
			#$obj->dblog("0","O Usuário administrador removeu do sistema os registros: $tmp_arr da tabela atividade.");
			$obj->alerta("Registro(s) removidos(s) com sucesso!","cliente.php");
		}else{
			#$obj->dblog('0',"O Usuário administrador tentou remover do sistema os registros: $tmp_arr da tabela atividade.");
			$obj->alerta('Erro ao remover registro','cliente.php');
		}
	}
?>