<?php
include("../../cabecalho.php");
$sys = new principal();
$obj = new clientes();
$sys->cabecalho();

?>
		<script>
			$(document).ready(function(){

			$("#cli_fon").mask("\(99\)9999-9999");
			$("#cli_cel").mask("\(99\)9999-9999");
			$("#cli_cep").mask("99999-999");
			$("#cli_nasc").mask("99/99/9999");
			$("#cli_datcad").mask("99/99/9999");

			//ação dos botões 
			$('#cadastra').click(function(){
			$("#cliente").attr("action", "executar.php?acao=add")
			$("#cliente").submit();
			});	
			$('#editar').click(function(){
			$("#cliente").attr("action", "executar.php?acao=edit")
			$("#cliente").submit();
			});	
			$('#delete').click(function(){
			$("#cliente").attr("action", "")
			$("#cliente").submit();
			});
			$('#cancelar').click(function(){
			$("#cliente").attr("action", "")
			$("#cliente").submit();
			});
			
			$('.ver_mask').click(function(){
			$('input:radio[name=rg_nacional]').each(function() {

				//Verifica qual está selecionado
			if ($(this).is(':checked'))
			valor = parseInt($(this).val());

			if ( valor == 1 ){
			$("#cli_rnacional").mask("999.999.999-99");
			}else{
			$("#cli_rnacional").mask("99.999.999/9999-99");
			}

			});           
			});


			});
			</script>
<div id="navcontainer">
<?  $sys->menu(); ?>
</div>

<form name="cliente" id="cliente" method="post">
	<div id="content">
	<div id="formulario" align="center">

		<table border="1">
			<tr align="center"><td><p><h1>Cliente</h1></td></tr>
			<tr>
				<td bgcolor="#f2f2f2" width="2%">Código:
				<input type="text" name="cod_cliente" maxlength="4" size="2" disabled >
				Nome/Razão Social: <input type="text" name="cli_nome" id="cli_nome" maxlength="30" size="21">
				Nome Fantasia: <input type="text" name="cli_nomeFantazia" id="cli_nomeFantazia" maxlength="20" size="11">
				CPF <input type="radio" class="ver_mask" id="cli_cpf" name="rg_nacional" value="1" >
				CNPJ<input type="radio" class="ver_mask" id="cli_cnpj" name="rg_nacional" value="2" >
				<input type="text" id="cli_rnacional" name="cli_rnacional" maxlength="18" size="11">
				ID/IE: <input type="text" name="cli_ins_est" id="cli_ins_est" maxlength="10" size="6">
				Insc. Municipal: <input type="text" name="cli_ins_mun" id="cli_ins_mun" maxlength="6" size="6">
				</td>
			</tr>
			<tr>
				<td bgcolor="#f2f2f2" width="2%">Sexo: <select name="cli_sexo" id="cli_sexo">
						<option>Selecione</option>
						<option value="mas">Masculino</option>
						<option value="fem">Feminino</option>
						</select>
					Contato:	<input type="text" name="cli_contato" id="cli_contato" maxlength="20" size="11">
					Atividade: <input type="text" name="cli_atividade" id="cli_atividade" maxlength="10" size="6">
					Orgão Público: <select name="cli_orpublico" id="cli_orpublico">
											<option>Selecione</option>
											<option value="mun">Municipal</option>
											<option value="est">Estadual</option>
											<option value="fed">Federal</option>
										</select>
					CEP: <input type="text" id="cli_cep" name="cli_cep" maxlength="10" size="6">
					Logradouro:	<input type="text" id="cli_logadouro" name="cli_logadouro" maxlength="35" size="26">
					Numero: <input type="text" id="cli_numero" name="cli_numero" maxlength="5" size="3">
				</td>
			</tr>	
			<tr>
				<td bgcolor="#f2f2f2" width="2%">Complemento:
					<input type="text" id="cli_compl" name="cli_compl" maxlength="30" size="21">
					Bairro: <input type="text" id="cli_bair" name="cli_bair" maxlength="30" size="21">
					UF: <select name="cli_uf" id="cliente_uf">
							 <option>Selecione</option>
							 <option value="AC">AC</option>
							 <option value="AL">AL</option>
							 <option value="AP">AP</option>
							 <option value="AM">AM</option>
							 <option value="BA">BA</option>
							 <option value="CE">CE</option>
							 <option value="ES">ES</option>
							 <option value="DF">DF</option>
							 <option value="MA">MA</option>
							 <option value="MT">MT</option>
							 <option value="MS">MS</option>
							 <option value="MG">MG</option>
							 <option value="PA">PA</option>
							 <option value="PB">PB</option>
							 <option value="PR">PR</option>
							 <option value="PE">PE</option>
							 <option value="PI">PI</option>
							 <option value="RJ">RJ</option>
							 <option value="RN">RN</option>
							 <option value="RS">RS</option>
							 <option value="RO">RO</option>
							 <option value="RR">RR</option>
							 <option value="SC">SC</option>
							 <option value="SP">SP</option>
							 <option value="SE">SE</option>
							 <option value="TO">TO</option>
						</select>	
				Cidade: <input type="text" id="cli_cid" name="cli_cid" maxlength="30" size="22">
				Fone: <input type="text" id="cli_fon" name="cli_fon" maxlength="9" size="5">
				celular: <input type="text" id="cli_cel" name="cli_cel" maxlength="9" size="5">		
				</td>							
			</tr>		
			<tr>	
				<td bgcolor="#f2f2f2" width="2%">email:
				<input type="text" id="cli_email" name="cli_email" maxlength="30" size="26">
			  	Data de Nascimento: <input type="time" id="cli_nasc" name="cli_nasc" maxlength="10" size="4">
			  	Cadastrado em: <input type="time" id="cli_datcad" name="cli_datcad" maxlength="10" size="4">
			  	</td>
			</tr>
		
	
	
		
			<tr>
				<td colspan="4" align="center"><br />
				<input type="button" class="button" name="novo" id="cadastra" value="Cadastrar" />
				<input type="button" class="button" name="novo" id="editar" value="Editar" />
				<input type="button" class="button" name="novo" id="delete" value="Excluir" />
				<input type="button" class="button" name="novo" id="cancelar" value="Cancelar" />
				<input type="button" class="button" name="novo" id="pesquisarr" value="Pesquisar" onClick="window.showModalDialog('pes_cli.php');"/>
				
				<br><br>
			</tr>
				
				
		</table>
	</div>
	</div>	
</form>
<?
$sys->rodape();
?>



